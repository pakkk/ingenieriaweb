var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('elearning', { login: 'Login' });
});

router.get('/contacto', function(req, res, next) {
  res.render('contacto', { login: 'Login' });
});

router.get('/elearning', function(req, res, next) {
  res.render('elearning', { login: 'Login' });
});

router.get('/idiomas', function(req, res, next) {
  res.render('idiomas', { login: 'Login' });
});

router.get('/oposiciones', function(req, res, next) {
  res.render('oposiciones', { login: 'Login' });
});

router.get('/login', function(req, res, next) {
  res.render('login', { login: 'Login' });
});

router.get('/contacto_realizado', function(req, res, next) {
  res.render('contacto_realizado', { login: 'Logged' });
});

router.get('/logged', function(req, res, next) {
  res.render('logged', { login: 'Login' });
});

module.exports = router;
